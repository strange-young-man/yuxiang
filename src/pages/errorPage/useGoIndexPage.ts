import { useSystemStore } from '@/store/system'
import { useRouter } from 'vue-router'

function useGoIndexPage() {
  const router = useRouter()
  const systemStore = useSystemStore()
  const goIndexPage = () => {
    router.push({
      name: systemStore.indexPageObj.name
    })
  }
  return { goIndexPage }
}

export default useGoIndexPage
