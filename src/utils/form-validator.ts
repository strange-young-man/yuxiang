/**
 * @author: clx
 * @description 表单验证
 */

import { Rule } from 'ant-design-vue/es/form';
import { Ref } from 'vue';

/**
 * 检验表单是否通过
 * @param formRef  表单Ref
 * @param needValidateData  布尔值，默认false。true返回validate返回的数据，false返回是否通过检验的布尔值
 */
export function formVerify(formRef: Ref): Promise<boolean>;
export function formVerify(formRef: Ref, needValidateData: boolean): Promise<object>;
export function formVerify(formRef: Ref, needValidateData: boolean = false): Promise<boolean | object> {
    if (needValidateData) {
        return formRef.value.validate()
    }
    return formRef.value.validate()
        .then(() => {
            return Promise.resolve(true)
        })
        .catch(() => {
            return Promise.resolve(false)
        })
}

// 定义表单校验Rules附加属性类型
type JoinRule = Rule & { custom: { [attr: string]: any } }

// 检验密码
export function validatePasswd(rule: JoinRule, value: string) {
    // 提取custom自定义传参
    const { formState, formRef } = rule?.custom || {}
    // 定义密码正则规则
    let re = /^[^\u4e00-\u9fa5]{6,16}$/
    if (value === '') {
        return Promise.reject('新密码不能为空');
    } else if (!re.test(value)) {
        return Promise.reject('密码不能包含汉字且不能小于6位，大于16位');
    } else {
        if (formState?.psdCheck !== '') {
            formRef?.value?.validateFields('psdCheck');
        }
        return Promise.resolve()
    }
};

// 检验确认密码
export function validatePassCheck(rule: JoinRule, value: string) {
    // 提取custom自定义传参
    const { formState } = rule?.custom || {}
    if (value === '') {
        return Promise.reject('确认密码不能为空');
    } else if (value !== formState.passwd) {
        return Promise.reject('确认密码与新密码不一致');
    } else {
        return Promise.resolve()
    }
};


export class FormValidator {
    constructor() { }



    // 清除全部状态
    // clearFormValidate = (that, name = "formValidate") => {
    //     that.$refs[name].fields.forEach((valid) => {
    //         valid.validateDisabled = '' //是否校验
    //         valid.validateState = '' //爆红状态
    //         valid.validateMessage = '' //爆红文字
    //     })
    // }
    // // 清除指定项状态
    // clearFormItemValidate = (that, prop, name = "formValidate") => {
    //     that.$refs[name].fields.forEach((valid) => {
    //         if (valid.prop == prop) {
    //             valid.validateDisabled = '' //是否校验
    //             valid.validateState = '' //爆红状态
    //             valid.validateMessage = '' //爆红文字
    //         }
    //     })
    // }
    // // 为指定项添加状态以及文字
    // handleFormItemValidateText = (that, prop, errText, name = "formValidate") => {
    //     that.$refs[name].fields.forEach((valid) => {
    //         if (valid.prop == prop) {
    //             valid.validateDisabled = false //是否校验
    //             valid.validateState = 'error' //爆红状态
    //             valid.validateMessage = errText //爆红文字
    //         }
    //     })
    //     return false
    // }
    // // 验证指定项状态
    // formItemValidate = (that, prop, name = "formValidate") => {
    //     let flag = false
    //     that.$refs[name].fields.forEach((valid) => {
    //         if (valid.prop == prop) {
    //             // console.log(valid);
    //             // valid.validateDisabled = '' //是否校验
    //             // valid.validateState = '' //爆红状态
    //             // valid.validateMessage = '' //爆红文字
    //             // let all = false
    //             // that.$refs[name].validate((valid) => {
    //             //     console.log("ccccccccccc==",valid,Boolean(valid));
    //             //     all = Boolean(valid)
    //             // })
    //             // return all
    //             that.$refs[name].validateField([prop], (Error) => {
    //                 if (!Error) {
    //                     flag = true
    //                 } else {
    //                     flag = false
    //                 }
    //             })
    //         }
    //     })
    //     return flag
    // }

    // // 密码
    // texTiPassword = (rule, value, callback) => {
    //     // let re = /^[^\u4e00-\u9fa5]{6,16}$/
    //     let re = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
    //     let { required } = this.ruleInfo(rule)
    //     if (!value && value !== 0) {
    //         required ? callback(new Error(`密码不能为空`)) : callback();
    //     } else if (!re.test(value)) {
    //         callback(new Error(`密码格式不正确`));
    //     } else {
    //         callback();
    //     }
    // }
    // // 确认密码
    // texTiPasswordAgain = (rule, value, callback) => {
    //     let { required, that } = this.ruleInfo(rule)
    //     if (!value && value !== 0) {
    //         required ? callback(new Error(`确认密码不能为空`)) : callback();
    //     } else if (value !== that.formCode.newPassword) {
    //         callback(new Error(`确认密码需要和新密码相同`));
    //     } else {
    //         callback();
    //     }
    // }




    // // 限制输入框内容最大长度和最小长度
    // limitLength = (rule, value, callback) => {
    //     console.log(rule)
    //     let maxLength = rule.maxLength;
    //     let minLength = rule.minLength;
    //     let { required, text } = this.ruleInfo(rule)
    //     console.log(required, text, maxLength, minLength)
    //     if (!value && value !== 0) {
    //         required ? callback(new Error(`${text}不能为空`)) : callback();
    //     } else if (value.indexOf(' ') !== -1) {
    //         callback(new Error(`${text}不能含有空格`));
    //     } else if (!(value.length <= maxLength && value.length >= minLength)) {
    //         callback(new Error(`${text}长度必须多于${minLength}少于${maxLength}`));
    //     } else {
    //         callback();
    //     }
    // }
}
const formValidator = new FormValidator()

export default formValidator