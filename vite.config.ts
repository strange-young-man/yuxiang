import { defineConfig, type PluginOption } from 'vite'
import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'
import { resolve } from 'path'
import { createStyleImportPlugin, VxeTableResolve } from 'vite-plugin-style-import'


// 自定义 svg 图标
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";

export default defineConfig({
  plugins: [
    vue(),
    // visualizer({
    //   // open: true
    // }) as PluginOption,
    createStyleImportPlugin({
      resolves: [VxeTableResolve()],
       // 配置图标路径
       iconDirs: [resolve(process.cwd(), "src/assets/icons")],
       // 图标id
       symbolId: "icon-[dir]-[name]",
    })
  ],
  base: './',
  server: {
    host: '0.0.0.0',
    open: true
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
      _c: resolve(__dirname, 'src/components')
    },
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue', '.wasm', '.svg', '.vue']
  },
  build: {
    outDir: 'previewOnline',
    chunkSizeWarningLimit: 500, // 设置打包分包大小警告阈值
    // terserOptions: {
    //   compress: false // 禁用压缩
    // },
    rollupOptions: {
      // 配置分包
      output: {
        manualChunks(id) {
          // 根据需要进行分包，这里以 lodash-es 作为例子
          if (id.includes('node_modules/lodash-es')) {
            return 'lodash-es'
          }
          if (id.includes('node_modules/mockjs')) {
            return 'mockjs'
          }
          if (id.includes('node_modules/ant-design-vue')) {
            return 'ant-design-vue'
          }
          if (id.includes('node_modules/vxe-table')) {
            return 'vxe-table'
          }
        }
      }
    }
  }
})
