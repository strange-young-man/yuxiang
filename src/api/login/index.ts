import { get, post } from '@/api/instance'

// 登录
export const loginApi = (params?: any, config?: any) => post('/api/login', params, config);