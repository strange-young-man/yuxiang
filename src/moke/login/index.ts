import Mock from 'mockjs'

// 设置延迟时间范围，单位为毫秒
Mock.setup({
  timeout: '100-1000'
})

Mock.mock('/api/login', 'post', function (data: any) {
  const { username, password } = JSON.parse(data.body)
  if (password !== '123456') {
    return {
      code: '401',
      msg: '密码错误！',
      data: null
    }
  }
  let userDetails = {}
  if (username === 'admin') {
    userDetails = {
      userName: '超级管理员',
      userId: '1',
      token: 'super_admin',
      roleId: '1',
      avatar: 'https://img1.baidu.com/it/u=1943243756,1413622420&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=501',
      menuId: '1',
      menuList: [
        {
          name: 'home',
          meta: {
            title: '首页'
          }
        },
        {
          name: 'library',
          meta: {
            title: '功能'
          },
          children: [
            {
              name: 'createAGame',
              meta: {
                title: 'axios取消请求'
              }
            }
          ]
        },
        {
          name: 'real_time_Data',
          meta: {
            title: '功能'
          },
          children: [
            {
              name: 'customModel',
              meta: {
                title: 'model弹窗'
              }
            },
            {
              name: 'test',
              meta: {
                title: 'test页面'
              }
            }
          ]
        }
      ]
    }
  } else {
    userDetails = {
      userName: '普通用户',
      userId: '2',
      token: 'general_admin',
      roleId: '2',
      avatar: 'https://img0.baidu.com/it/u=3123360163,3557638406&fm=253&fmt=auto&app=138&f=JPG?w=500&h=544',
      menuId: '2',
      menuList: [
        {
          name: 'library',
          meta: {
            title: '功能'
          },
          children: [
            {
              name: 'createAGame',
              meta: {
                title: 'axios取消请求'
              }
            },
            {
              name: 'customModel',
              meta: {
                title: 'model弹窗'
              }
            },
            {
              name: 'test',
              meta: {
                title: 'test页面'
              }
            }
          ]
        }
      ]
    }
  }
  return {
    code: 1,
    msg: '登录成功！',
    data: userDetails
  }
})
