import { get, post } from '@/api/instance'

// axios取消请求
export const axiosAbortNetwork = (params?: any, config?: any) => post('https://api.uomg.com/api/rand.qinghua', params, config)

// axios限制并发
export const axiosConcurrentNetwork = (params?: any, config?: any) => post('https://api.uomg.com/api/rand.qinghua', params, config)
