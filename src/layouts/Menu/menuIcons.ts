import { HomeOutlined, CloudSyncOutlined } from '@ant-design/icons-vue'
import { h, defineComponent } from 'vue'

export const menuIcons = {
  HomeOutlined,
  CloudSyncOutlined
}

export type MenuIconsTypes = keyof typeof menuIcons

const MenuIconsComp = defineComponent(
  props => {
    return () => {
      const Icon = menuIcons[props.type as MenuIconsTypes]
      return h(Icon)
    }
  },
  {
    props: {
      type: {
        type: String,
        required: true
      }
    }
  }
)

export default MenuIconsComp
