import { createApp } from 'vue'
import './style.less'
import App from './App.vue'
import router from '@/router/index'
import Antd from 'ant-design-vue'
import store from './store'
// 不需要moke时删除此代码并删除moke文件夹即可
import '@/moke/index.ts'
// 引入VXETable
import { Tooltip as VXETooltip, Column as VXEColumn, Table as VXEtable } from 'vxe-table'
import 'vxe-table/lib/style.min.css'

// 创建app
const app = createApp(App)

// 按需挂载VXETable
function useVXETable(app: any) {
  app.use(VXETooltip).use(VXEColumn).use(VXEtable)
}

// 挂载
app.use(useVXETable).use(Antd).use(store).use(router).mount('#app')
