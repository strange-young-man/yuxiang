import Layouts from '@/layouts/index.vue'

import { MenuIconsTypes } from '@/layouts/Menu/menuIcons'
interface IconTreeNode {
  [key: string]: any
  meta?: {
    [key: string]: any
    icon?: MenuIconsTypes
  }
  children?: IconTreeNode[]
}

/**
 * meta: {
 *   title: 显示在侧边栏、面包屑和标签栏的文字使用
 *   hideInBread: 是否在面包屑中隐藏 布尔值
 *   hideInMenu: 是否在左侧菜单中隐藏 布尔值
 *   icon: antd-icon
 *   href: 跳转链接
 * }
 */

const routes: IconTreeNode[] = [
  {
    path: '/:pathMatch(.*)',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/pages/errorPage/404/index.vue')
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/pages/login/index.vue')
  },
  {
    path: '/',
    component: Layouts,
    redirect: '/login',
    children: [
      {
        path: 'home',
        name: 'home',
        meta: {
          title: '首页',
          icon: 'HomeOutlined'
        },
        component: () => import('@/pages/home/index.vue')
      },
      {
        path: '/library',
        name: 'library',
        meta: {
          icon: 'CloudSyncOutlined',
          title: '游戏管理'
        },
        children: [
          {
            path: 'createAGame',
            name: 'createAGame',
            meta: {
              title: '创建游戏',
              icon: 'CloudSyncOutlined'
            },
            component: () => import('@/pages/createAGame/index.vue')
          },
          {
            path: 'concurrentNetwork',
            name: 'concurrentNetwork',
            meta: {
              title: '安装包资源',
              icon: 'CloudSyncOutlined'
            },
            component: () => import('@/pages/concurrentNetwork/index.vue')
          },
          {
            path: 'listOfGames',
            name: 'listOfGames',
            meta: {
              title: '游戏列表',
              icon: 'CloudSyncOutlined'
            },
            component: () => import('@/pages/listOfGames/index.vue')
          },
        ]
      },
       {
        path: '/real_time_Data',
        name: 'realTimeData',
        meta: {
          icon: 'CloudSyncOutlined',
          title: '数据看板'
        },
        children: [
          {
            path: 'customModel',
            name: 'customModel',
            meta: {
              title: '实时数据',
              icon: 'CloudSyncOutlined'
            },
            component: () => import('@/pages/customModel/index.vue')
          },
          {
            path: 'test',
            name: 'test',
            meta: {
              title: '数据分析',
              icon: 'CloudSyncOutlined'
            },
            component: () => import('@/pages/test/index.vue')
          }
        ]
      },
      {
        path: 'multilevel',
        name: 'multilevel',
        meta: {
          title: '多级菜单',
          icon: 'CloudSyncOutlined'
        },
        children: [
          {
            path: 'level-2-1',
            name: 'level-2-1',
            meta: {
              icon: 'CloudSyncOutlined',
              title: '二级-1'
            },
            component: () => import('@/pages/multilevel/level-2-1.vue')
          },
          {
            path: 'level-2-2',
            name: 'level-2-2',
            meta: {
              icon: 'CloudSyncOutlined',
              title: '二级-2'
            },
            children: [
              {
                path: 'level-2-2-1',
                name: 'level-2-2-1',
                meta: {
                  icon: 'CloudSyncOutlined',
                  title: '二级-2-1'
                },
                component: () => import('@/pages/multilevel/level-2-2/level-2-2-1.vue')
              },
              {
                path: 'level-2-2-2',
                name: 'level-2-2-2',
                meta: {
                  icon: 'CloudSyncOutlined',
                  title: '二级-2-2'
                },
                component: () => import('@/pages/multilevel/level-2-2/level-2-2-2.vue')
              }
            ]
          },
          {
            path: 'level-2-3',
            name: 'level-2-3',
            meta: {
              icon: 'CloudSyncOutlined',
              title: '二级-3'
            },
            children: [
              {
                path: 'level-2-3-1',
                name: 'level-2-3-2',
                meta: {
                  icon: 'CloudSyncOutlined',
                  title: '二级-3-1'
                },
                component: () => import('@/pages/multilevel/level-2-3/level-2-3-1.vue')
              }
            ]
          },
          {
            path: 'level-2-4',
            name: 'level-2-4',
            meta: {
              icon: 'CloudSyncOutlined',
              title: '二级-4'
            },
            component: () => import('@/pages/multilevel/level-2-4.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/pages/errorPage/404/index.vue')
  },
  {
    path: '/403',
    name: '403',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/pages/errorPage/403/index.vue')
  }
]
export default routes
