import { storeToRefs } from 'pinia'
import { useSystemStore } from '@/store/system'
import { ItemType } from 'ant-design-vue'
import routes from '@/router/routes'
import config from '@/config'
import { menuIcons } from './menuIcons.ts'
import { computed, h } from 'vue'

function useGetMenuList() {
  const { menuData } = storeToRefs(useSystemStore())
  const menuList = computed(() => {
    return (config.useLocRouter ? getLocRoutes() : getHandleApiRoutes(menuData.value)) || []
  })
  return { menuList }
}

const getIconVueNode = (item: any) => {
  const { icon }: { icon: keyof typeof menuIcons } = item?.meta
  let iconVueNode
  if (icon) {
    iconVueNode = menuIcons[icon] ? () => h(menuIcons[icon]) : undefined
  }

  return iconVueNode
}

// 获取本地路由
const getLocRoutes = (): ItemType[] => {
  // 匹配路由数组数据
  const matchMenu = (list: any[]): ItemType[] => {
    let resMenu: any[] = []
    list.forEach(item => {
      if (item.meta && !item.meta.hideInMenu) {
        const { name, path } = item
        const { icon, title, href } = item.meta
        // 配置属性
        let obj: { [atrr: string]: any } = {
          key: item.name,
          icon: getIconVueNode(item),
          title: title || name,
          label: title || name,
          path,
          name,
          href
        }
        // 配置children
        if (Array.isArray(item.children) && item.children.length) {
          obj.children = matchMenu(item.children)
        }
        // 放进数组
        resMenu.push(obj)
      }
    })
    return resMenu
  }
  // path:/ 内的路由数据
  const rootRoutes: any[] = routes.find(item => item.path == '/')?.children || []
  // 获取最终菜单
  const menuItems = matchMenu(rootRoutes)
  console.log(menuItems,'menuItems---11')

  return menuItems
}

// 获取并处理接口动态路由
const getHandleApiRoutes = (menuListFromApi: any[]): ItemType[] => {
  const iconsObj = getLocRoutesIcons()

  // 匹配路由数组数据
  const matchMenu = (list: any[]): ItemType[] => {
    let resMenu: any[] = []
    list.forEach(item => {
      if (item.meta && !item.meta.hideInMenu) {
        const { name } = item
        const { title } = item.meta
        item.meta.icon = iconsObj[name]
        // 配置属性
        let obj: { [atrr: string]: any } = {
          key: item.name,
          icon: getIconVueNode({ ...item }),
          title: title || name,
          label: title || name,
          name
        }
        // 配置children
        if (Array.isArray(item.children) && item.children.length) {
          obj.children = matchMenu(item.children)
        }
        // 放进数组
        resMenu.push(obj)
      }
    })
    return resMenu
  }
  // 获取最终菜单
  const menuItems = matchMenu(menuListFromApi)
  return menuItems
}

// 获取路由icon
const getLocRoutesIcons = () => {
  const getIcons = (list: any[], icons: { [attr: string]: any } = {}) => {
    list.forEach(item => {
      if (item.meta && item.meta.icon) {
        const { name } = item
        const { icon } = item.meta
        // 配置属性
        icons[name] = icon

        // 配置children
        if (Array.isArray(item.children) && item.children.length) {
          getIcons(item.children, icons)
        }
      }
    })
    return icons
  }

  // path:/ 内的路由数据
  const rootRoutes: any[] = routes.find(item => item.path == '/')?.children || []
  // 获取最终菜单
  const iconsObj = getIcons(rootRoutes)
  return iconsObj
}

export default useGetMenuList
