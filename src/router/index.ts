import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes'
import guards from './guards'

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

guards.beforeEach.map((guard) => {
    router.beforeEach(guard);
});

guards.afterEach.map((guard) => {
    router.afterEach(guard);
});

export default router