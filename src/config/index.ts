interface configInterface {
  title: string
  useLocRouter: boolean
  maxProgressCount: number | null
}
const config: configInterface = {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: 'iviewAdminBase',
  /**
   * @description 是否使用本地路由 false为使用动态路由
   */
  useLocRouter: true,
  // useLocRouter: false,
  /**
   * @description axios网络请求最大并发数量 null为不限制
   */
  maxProgressCount: 2
}

export default config
